<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * Public Scopes begin
     *
     * Default flag is true
     *
     **/
    // Return featured images
    public function scopeFeatured($query, $flag = 1)
    {
        return $query->where('featured', $flag);
    }

    // Return the image's auction
    public function auction()
    {
        return $this->hasOne('App\Auction', 'id', 'auction_id')->first();
    }
}
