<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Businessunit extends Model
{
    use SoftDeletes;

    // Return the auctions associated to this Business Unit
    public function auctions()
    {
        return $this->hasOne('App\Auction')->get();
    }
}
