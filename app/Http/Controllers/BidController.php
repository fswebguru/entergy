<?php

namespace App\Http\Controllers;

use App\Bid;
use Illuminate\Support\Facades\Auth;

class BidController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $bids = Bid::where('user_id', $user->id)->get();
        $data = $bids->map(function ($bid) { // add the expired_at of auction in a given bid
            $bid['auction_created_at'] = $bid->auction()->created_at;
            return $bid;
        });
        $filtered_bids = $data->sortBy('amount')
                ->keyBy('auction_id')
                ->values();
        $sorted_bids = $filtered_bids->sortByDesc('auction_created_at');
        return view('layouts.user_bids')
                ->with('bids', $sorted_bids);
    }

    public function destroy($id)
    {
        $bid = Bid::find($id);
        $auction_id = $bid->auction_id;
        $bid->delete();
        return redirect()->action('AuctionController@show', ['id' => $auction_id]);
    }

    public function winner($id)
    {
        $bid = Bid::find($id);
        $auction = $bid->auction();
        if ($auction->winnerBid() != null) {
            $oldwinner = $auction->winnerBid();
            $oldwinner->winner = false;
            $oldwinner->save();
        }
        $bid->winner = true;
        $bid->save();
        return redirect()->action('AuctionController@show', ['id' => $bid->auction_id]);
    }

}
