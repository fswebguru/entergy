<?php

namespace App\Http\Controllers;

use App\Auction;
use Carbon\Carbon;
use App\Category;
use App\Businessunit;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
        //
        $allcount = Auction::all()->count();
        $categories = Category::all();
        $auctions = Auction::all();
        return view('layouts.admin_dashboard')
                ->with('auctions', $auctions)
                ->with('categories', $categories)
                ->with('allcount', $allcount);
    }
}
