<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailOutBidAuction extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $auction_name, $bid_amount)
    {
        $this->user = $user;
        $this->auction_name = $auction_name;
        $this->bid_amount = $bid_amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.auctions.outbid')
            ->subject('You\'ve been outbid from the auction "' . $this->auction_name . '"')
            ->with('auction_name' , $this->auction_name)
            ->with('bid_amount' , $this->bid_amount);
    }
}
