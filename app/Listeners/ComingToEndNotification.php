<?php

namespace App\Listeners;

use Aloha\Twilio\Twilio;
use App\Events\AuctionComingToEnd;
use App\Mail\EmailAuctionComingToEnd;
use App\Watch;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;

class ComingToEndNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuctionComingToEnd  $event
     * @return void
     */
    public function handle(AuctionComingToEnd $event)
    {
        // get all watches relevant to this auction
        $watches = Watch::where('auction_id', $event->auction->id)->get();

        // Emails all users about this auction is about to end within an hour
        foreach ($watches as $watch) {
            $user = $watch->owner();
            $auction_name = $event->auction->item_name;
            $bid_amount = $event->auction->highestbid()->amount;

            // Send an email if checked 'notify_email' option
            if ($watch->notify_email) {
                $email = new EmailAuctionComingToEnd($user, $auction_name, $bid_amount);
                Mail::to($user->email)->send($email);
            }

            // Send a sms if checked 'notify_phone' option
            if ($watch->notify_phone && !is_null($user->phone)) {
                $message = "Bidding ends for the auction \"" . $auction_name . "\" within an hour. The bidded highest price is $" . $bid_amount . " - Entergy Bid Board. "
                            . "Visit the Entergy Bid Board for more information. "
                            . "To unsubscribe, visit the auction page and disable the Alerts.";
                $accountId = config('twilio.twilio.connections.twilio.sid');
                $token = config('twilio.twilio.connections.twilio.token');
                $fromNumber = config('twilio.twilio.connections.twilio.from');
                $twilio = new Twilio($accountId, $token, $fromNumber);
                try {
                    $twilio->message($user->phone, $message);
                } catch (\Services_Twilio_RestException $e) {
                    Log::error(
                        'Could not send SMS notification.' .
                        ' Twilio replied with Services_Twilio_RestException: The \'To\' number ' . $user->phone . ' is not a valid phone number.'
                    );
                }

                // Test the sms process via Nexmo on the local env
                /*
                Nexmo::message()->send([
                    'to' => 'user_number',
                    'from' => 'fake_number',
                    'text' => $message
                ]);
                */
            }
        }
    }
}
