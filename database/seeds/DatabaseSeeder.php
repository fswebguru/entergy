<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        App\User::create(array('name' => 'Admin', 'email' => 'fooadmin@entergy.com', 'password' => '$2y$10$ZxnqD5aag4wXEs4aX3vJn..oTSz5sFJArSvcD8ByMRSgSDDsRoUty', 'verified' => true, 'admin' => true));
        App\User::create(array('name' => 'Regular User', 'email' => 'foouser@entergy.com', 'password' => '$2y$10$ZxnqD5aag4wXEs4aX3vJn..oTSz5sFJArSvcD8ByMRSgSDDsRoUty', 'verified' => true));
        DB::table('categories')->delete();
        App\Category::create(array('name' => 'Electronics'));
        App\Category::create(array('name' => 'Computers'));
        App\Category::create(array('name' => 'Vehicles'));
        App\Category::create(array('name' => 'Furniture'));
        App\Category::create(array('name' => 'Equipment'));
        App\Category::create(array('name' => 'Buildings'));
        App\Category::create(array('name' => 'Power Equipment'));
        App\Category::create(array('name' => 'Other'));
        DB::table('businessunits')->delete();
        App\Businessunit::create(array('name' => 'Transportation'));
        App\Businessunit::create(array('name' => 'HR'));
        App\Businessunit::create(array('name' => 'IT'));
        DB::table('auctions')->delete();
        App\Auction::create(array('item_name' => 'Brother Laser Printer', 'item_description' => 'Brother laser printer in good condition. Works great. it is not cracked. Touch works 100%. Prints 100 pages a minute. No ink included.', 'item_condition' => 'Good', 'contact_name' => 'Foo Name', 'contact_phone' => '555-555-5555', 'expires_at' => Carbon::now()->addDays(30)->toDateTimeString(), 'user_id' => 1, 'category_id' => 1, 'businessunit_id' => 1, 'location' => 'New Orleans, LA, United States'));
        App\Auction::create(array('item_name' => 'Titanium Tibone Claw Hammer', 'item_description' => 'Titanium Tibone Claw Hammer in good condition. Works great. it is not cracked. It has cracked some nails', 'item_condition' => 'Poor', 'contact_name' => 'Foo Name', 'contact_phone' => '555-555-5555', 'expires_at' => Carbon::now()->addMinutes(70)->toDateTimeString(), 'user_id' => 1, 'category_id' => 1, 'businessunit_id' => 1, 'location' => 'New Orleans, LA, United States'));
        App\Auction::create(array('item_name' => 'Elliptical Bike Trainer Exercise Fitness Machine', 'item_description' => 'Build your legs up on this great stationary bike. TV not included', 'item_condition' => 'Like New', 'contact_name' => 'Foo Name', 'contact_phone' => '555-555-5555', 'expires_at' => Carbon::now()->subDays(2)->toDateTimeString(), 'user_id' => 1, 'category_id' => 4, 'businessunit_id' => 2, 'location' => 'New Orleans, LA, United States', 'ended_at' => Carbon::now()->subDays(2)->toDateTimeString(), 'buyer_name' => 'Other User'));
        App\Auction::create(array('item_name' => 'Auction to expire in 3 minutes', 'item_description' => 'This auction is for testing cronned batch close', 'item_condition' => 'Like New', 'contact_name' => 'Foo Name', 'contact_phone' => '555-555-5555', 'expires_at' => Carbon::now()->addMinutes(150)->toDateTimeString(), 'user_id' => 1, 'category_id' => 2, 'businessunit_id' => 3, 'location' => 'New Orleans, LA, United States'));
        App\Auction::create(array('item_name' => 'Auction to expire in 5 minutes', 'item_description' => 'This auction expires in 5 minutes and has bids, it should select the highest as winner', 'item_condition' => 'Like New', 'contact_name' => 'Foo Name', 'contact_phone' => '555-555-5555', 'expires_at' => Carbon::now()->addMinutes(10)->toDateTimeString(), 'user_id' => 1, 'category_id' => 3, 'businessunit_id' => 3, 'location' => 'New Orleans, LA, United States'));
        DB::table('bids')->delete();
        App\Bid::create(array('auction_id' => 1, 'user_id' => 2, 'amount' => 20));
        App\Bid::create(array('auction_id' => 2, 'user_id' => 1, 'amount' => 12));
        App\Bid::create(array('auction_id' => 2, 'user_id' => 2, 'amount' => 15));
        App\Bid::create(array('auction_id' => 3, 'user_id' => 2, 'amount' => 105));
        App\Bid::create(array('auction_id' => 3, 'user_id' => 1, 'amount' => 110));
        App\Bid::create(array('auction_id' => 3, 'user_id' => 2, 'amount' => 120, 'winner' => true));
        App\Bid::create(array('auction_id' => 5, 'user_id' => 1, 'amount' => 100));
        App\Bid::create(array('auction_id' => 5, 'user_id' => 1, 'amount' => 120));
        App\Bid::create(array('auction_id' => 5, 'user_id' => 1, 'amount' => 150));
        App\Bid::create(array('auction_id' => 5, 'user_id' => 2, 'amount' => 110));
        App\Bid::create(array('auction_id' => 5, 'user_id' => 2, 'amount' => 130));
        DB::table('watches')->delete();
        App\Watch::create(array('user_id' => 1, 'auction_id' => 2, 'notify_email' => true));
        App\Watch::create(array('user_id' => 2, 'auction_id' => 1, 'notify_email' => true, 'notify_phone' => true));
        App\Watch::create(array('user_id' => 1, 'auction_id' => 3, 'notify_email' => true));
        App\Watch::create(array('user_id' => 2, 'auction_id' => 2, 'notify_email' => true));
        App\Watch::create(array('user_id' => 2, 'auction_id' => 3, 'notify_email' => true, 'notify_phone' => true));
        DB::table('category_watches')->delete();
        App\CategoryWatch::create(array('user_id' => 1, 'category_id' => 1, 'notify_email' => true));
        App\CategoryWatch::create(array('user_id' => 2, 'category_id' => 1, 'notify_email' => true, 'notify_phone' => true));
        DB::table('images')->delete();
        App\Image::create(array('path' => 'public/auction_images/printer.jpg', 'featured' => 1, 'auction_id' => 1));
        App\Image::create(array('path' => 'public/auction_images/printer2.jpeg', 'featured' => 0, 'auction_id' => 1));
        App\Image::create(array('path' => 'public/auction_images/hammer.jpg', 'featured' => 1, 'auction_id' => 2));
        App\Image::create(array('path' => 'public/auction_images/eliptical.jpg', 'featured' => 1, 'auction_id' => 3));
    }
}
