<div class="col-md-4">
  <div class="item clearfix">
    @if ($auction->ended_at)
      <span class="status bg-warning">Ended</span>
    @endif
    <a href="{{ route('auction.detail', ['id' => $auction->id]) }}">
      <img class="img-responsive" src="{{ isset($auction->featuredImage()->path) ? $auction->featuredImage()->path : 'assets/default.png' }}">
    </a>
    <footer>
      <p>
        @if ($auction->alert_property == 'outbid')
        <a href="{{ route('auction.detail', ['id' => $auction->id]) }}" class="label label-danger">You were outbid</a>
        @elseif ($auction->alert_property == 'close')
        <a href="{{ route('auction.detail', ['id' => $auction->id]) }}" class="label label-warning">About to close</a>
        @elseif ($auction->alert_property == 'close_outbid')
        <a href="{{ route('auction.detail', ['id' => $auction->id]) }}" class="label label-warning">About to close</a>
        <a href="{{ route('auction.detail', ['id' => $auction->id]) }}" class="label label-danger">You were outbid</a>
        @else
        <a href="{{ route('auction.detail', ['id' => $auction->id]) }}" class="label label-default">Others</a>
        @endif
      </p>
      <h4>
        <a href="{{ route('auction.detail', ['id' => $auction->id]) }}">
          {{ $auction->item_name }}<br>
          ${{ isset($auction->highestbid()->amount) ? $auction->highestbid()->amount : 0 }}
        </a>
      </h4>
      <p class="text-light">
        {{ $auction->bids->count() }} bids &nbsp; {{ $auction->timeRemain() }}
      </p>
    </footer>
  </div>
</div>
