<div class="modal fade" id="bidModal" tabindex="-1" role="dialog" aria-labelledby="Make a Bid">
  <div class="modal-dialog" role="document">
    <div class="modal-content  dark-bg">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Bid for {{ $auction->item_name }}</h4>
      </div>
      <div class="modal-body light-bg">
        <form class="form-horizontal" id="bidModalForm" method="POST" action="{{ Request::url() }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="sales_number" class="col-sm-4 control-label">IR Number</label>
            <div class="col-sm-8">
              <span style="display: inline-block;  padding-top: .5em;">{{ $auction->getIR() }}</span>
            </div>
          </div>
          <div class="form-group">
            <label for="bid_price" class="col-sm-4 control-label">Your bid</label>
            <div class="col-sm-8">
              <div class="input-group">
                <div class="input-group-addon">$</div>
                <input type="number" name="bid_price" id="bid_price" class="form-control input-lg" readonly>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="terms" class="col-sm-4 control-label">Terms</label>
            <div class="col-sm-8">
              <ol style="padding-left: 15px; padding-top: .5em;">
                <li>Buyer must be able to pick up item from site. All items sold on the employee bid board are sold "AS IS", "WHERE IS" and there are no implied
                  or expressed warranties.
                </li>
                <li>Buyer bears all responsibilities. Investment Recovery Services reserves the right to discontinue or reject any bids at any time during this
                  process.
                </li>
              </ol>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
              <div class="checkbox">
                <label>
                  <input type="checkbox" id="terms_checkbox"> I agree to the above terms
                </label>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <!-- FIXME - THIS SHOULD DISABLED until user checks the agree box above -->
        <input type="submit" value="Submit Bid" id="submit_bidForm" class="btn btn-success btn-lg" disabled>
      </div>
    </div>
  </div>
</div>
