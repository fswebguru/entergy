<nav class="navbar navbar-default">
  <div class="collapse navbar-collapse" style="padding-left: 0; padding-right: 0;">
    <ul class="nav navbar-nav">
      <li @if (!isset($activeId)) class="active" @endif>
        <a href="{{ action('AuctionController@index') }}">
          All items <span class="badge">{{ $allcount }}</span>
        </a>
      </li>
      @foreach($categories as $category)
        @include('user.category')
      @endforeach
    </ul>
  </div>
</nav>
