<div class="modal fade modal-emails" id="AuctionCloseModal" tabindex="-1" role="dialog" aria-labelledby="Email Winner">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Close Auction</h4>
      </div>
      <div class="modal-body dark-bg">
        <div class="row">
          <div class="col-sm-12">
            {!! BootForm::open()->action(URL::route('auctions.close')) !!}
            {!! BootForm::hidden('id', 'id')->value($auction->id) !!}
            {!! BootForm::select('Select a winner', 'bid_id', $auction->selectWinnerOptions()) !!}
            {!! BootForm::submit('Confirm and Close Auction', 'btn btn-lg btn-block btn-danger btn-admin') !!}
            {!! BootForm::close() !!}
            <br/>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
