<div class="clearfix spacer"></div>
<h1>Edit Auction</h1>

<hr>
<div class="row">
  <div class="col-sm-10 col-md-8 col-lg-7">
    {!! BootForm::openHorizontal(['sm' => [4, 8]])->action(URL::route('auctions.update', $auction->id))->put( )->multipart(); !!}
    {!! BootForm::bind($auction) !!}
    {!! BootForm::text('Item name', 'item_name')->addGroupClass('form-group-lg')->placeholder('Item name...') !!}
    {!! BootForm::text('IR number', 'ir_number')->placeholder('IR170031') !!}
    {!! BootForm::InputGroup('Minimum bid ','min_bid')->beforeAddon('$') !!}
    {!! BootForm::InputGroup('End date', 'end_date')->id('datepicker')
                  ->beforeAddon('<span class=\'glyphicon glyphicon-calendar\' aria-hidden=\'true\'></span>')
                  ->placeholder(Carbon\Carbon::parse($auction->expires_at)->timezone('America/Chicago')
                  ->format('n/j/Y'))->value(Carbon\Carbon::parse($auction->expires_at)
                  ->timezone('America/Chicago')->format('n/j/Y')) !!}
    {!! BootForm::InputGroup('End time', 'end_time')
                  ->beforeAddon('<span class=\'glyphicon glyphicon-time\' aria-hidden=\'true\'></span>')
                  ->value(Carbon\Carbon::parse($auction->expires_at)
                  ->timezone('America/Chicago')->format('H:i')) !!}
    {!! BootForm::textarea('Description', 'item_description')->placeholder('Add description here...') !!}
    {!! BootForm::select('Condition', 'item_condition', $condition_options) !!}
    {!! BootForm::select('Category', 'category_id', $categories->pluck('name', 'id')) !!}
    {!! BootForm::select('Business Unit', 'businessunit_id', $businessunits->pluck('name', 'id')) !!}
    <hr>
    {!! BootForm::text('Location ','location')->id('searchLocation')->placeholder('Address') !!}
    {!! BootForm::text('Contact name ','contact_name') !!}
    {!! BootForm::text('Contact phone ','contact_phone')->placeholder('555-555-5555') !!}
    @if ($auction->ended_at != null)
      <hr>
      {!! BootForm::text('Buyer name ','buyer_name')->placeholder('To substitute in the Bill of Sale') !!}
    @endif
    <hr>
    {!!BootForm::file('Add photos ','item_images[]')->multiple(true) !!}
    <hr>
    <div class="spacer"></div>
    {!! BootForm::submit('Edit Auction', 'btn btn-lg btn-block btn-success') !!}
    {!! BootForm::close() !!}
    @if ($auction->images()->count() > 0)
      <hr>
      <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10">
          <h2>Edit Images</h2>
          {!! BootForm::open()->addClass('image-edition'); !!}
          @each('admin.auctions_edit_images', $auction->images(), 'image')
          {!! BootForm::close() !!}
        </div>
      </div>
    @endif
  </div>
</div>

<div class="modal fade modal-auction-delete" id="auctionDeleteModal" tabindex="-1" role="dialog" aria-labelledby="Auction Delete">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Information</h4>
      </div>
      <div class="modal-body dark-bg">
        <p>In order to delete this photo, please first select another photo as the featured photo.</p>
      </div>
    </div>
  </div>
</div>
