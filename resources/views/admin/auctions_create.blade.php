<div class="clearfix spacer"></div>
<h1>New Auction</h1>

<hr>
<div class="row">
  <div class="col-sm-10 col-md-8 col-lg-7">
    {!! BootForm::openHorizontal(['sm' => [4, 8]])->action(URL::route('auctions.store'))->multipart(); !!}
    {!! BootForm::text('Item name', 'item_name')->addGroupClass('form-group-lg')->placeholder('Item name...') !!}
    {!! BootForm::text('IR number', 'ir_number')->placeholder('IR170031') !!}
    {!! BootForm::InputGroup('Minimum bid ','min_bid')->beforeAddon('$') !!}
    {!! BootForm::InputGroup('End date', 'end_date')->id('datepicker')
                  ->beforeAddon('<span class=\'glyphicon glyphicon-calendar\' aria-hidden=\'true\'></span>')
                  ->placeholder(Carbon\Carbon::createFromDate()->addWeek()->format('n/j/Y')) !!}
    {!! BootForm::InputGroup('End time', 'end_time')
                  ->beforeAddon('<span class=\'glyphicon glyphicon-time\' aria-hidden=\'true\'></span>')
                  ->value('19:00') !!}
    {!! BootForm::textarea('Description', 'item_description')->placeholder('Add description here...') !!}
    {!! BootForm::select('Condition', 'item_condition', $condition_options) !!}
    {!! BootForm::select('Category', 'category_id', $categories->pluck('name', 'id')) !!}
    {!! BootForm::select('Business Unit', 'businessunit_id', $businessunits->pluck('name', 'id')) !!}
    <hr>
    {!! BootForm::text('Location ','location')->id('searchLocation')->placeholder('Address') !!}
    {!! BootForm::text('Contact name ','contact_name') !!}
    {!! BootForm::text('Contact phone ','contact_phone')->placeholder('555-555-5555') !!}
    <hr>
    {!!BootForm::file('Add photos ','item_images[]')->multiple(true) !!}
    <div id="tryDz"></div>
    <hr>
    <div class="spacer"></div>
    {!! BootForm::submit('Start Auction', 'btn btn-lg btn-block btn-success') !!}
    {!! BootForm::close() !!}
    <hr>
    <div class="spacer"></div>
  </div>
</div>
