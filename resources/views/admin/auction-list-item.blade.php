<tr>
  <td data-order="{{$auction->id}}"><a href="{{ action('AuctionController@show', ['id' => $auction->id]) }}">{{$auction->id}}</a></td>
  <td>{{ $auction->getIR() }}</td>
  <td>
    <a href="{{ action('AuctionController@show', ['id' => $auction->id]) }}">
      <img class="img-responsive" src="/{{ isset($auction->featuredImage()->path) ? $auction->featuredImage()->path : 'assets/default.png' }}">
    </a>
  </td>
  <td>{{ $auction->winnerBid() != null ? $auction->winnerBid()->author()->email : ' - ' }}</td>
  <td>{{ $auction->item_name }}</td>
  <td>{{ $auction->category()->name }}</td>
  <td>{{ $auction->Businessunit()->name }}</td>
  <td>{{$auction->bids()->count()}}</td>
  <td>{{ isset($auction->ended_at) ? Carbon\Carbon::parse($auction->ended_at)->timezone('America/Chicago') : Carbon\Carbon::parse($auction->expires_at)->timezone('America/Chicago') }}</td>
  <td>${{ isset($auction->bids()->orderBy('amount', 'DESC')->first()->amount) ? $auction->bids()->orderBy('amount', 'DESC')->first()->amount : '0.00' }}</td>
</tr>
