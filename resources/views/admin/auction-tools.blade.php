<div class="row">
  <div class="col-xs-12">
    <a href="{{ action('AuctionController@edit', ['id' => $auction->id]) }}" class="btn btn-warning">Edit Auction</a>
    @if ($auction->ended_at != null && $auction->winnerBid() != null)
    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#emailWinnerModal">Email Winner <span class="caret"></span></a>
    @endif
    @if ($auction->ended_at == null)
    <a href="#" class="btn btn-danger btn-admin" data-toggle="modal" data-target="#AuctionCloseModal">Close Auction</a>
    @endif
  </div>
</div>
<hr>
