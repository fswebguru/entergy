<div class="col-sm-4 image-button-group">
  {!! BootForm::radio('<img src="/'.$image->path.'" class=\'img-responsive\'>', '1')
                ->name('img_sel')
                ->addClass('image-feature')
                ->value($image->id)
                ->defaultCheckedState($image->featured)
                ->data('auc_id', $image->auction_id) !!}
  {!! BootForm::button('Delete')->addClass('image-delete')->data('img_id', $image->id)!!}
</div>
