@extends('layouts.app')

@section('content')
  <h4>Next Step: <strong class="text-danger">Check Your Email</strong></h4>
  <p>
    You will receive a confirmation email within 5 minutes. That email will include a <strong>four-digit confirmation code</strong>.
    <strong>You must enter the code from the confirmation email in the form below to complete the sign up process</strong>.
    If you do not see a confirmation email check your spam folder.
  </p>

  <div class="spacer"></div>

  <div class="panel panel-default">
    <div class="panel-heading"><strong>Enter Confirmation Code</strong></div>
    <div class="panel-body">
      <form role="form" method="POST" action="{{ route('register.verify') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('verification_code') ? ' has-error' : '' }}">
          <input id="verification_code" type="number" class="form-control input-lg" name="verification_code" value="{{ old('verification_code') }}" placeholder="4 digit code" required autofocus>
          @if ($errors->has('verification_code'))
          <span class="help-block">
            <strong>{{ $errors->first('verification_code') }}</strong>
          </span>
          @endif
        </div>

        <div class="form-group">
          <p>
            <button type="submit" class="btn btn-success btn-block btn-lg">Confirm Identity</button>
          </p>
          <a href="#" class="plain">Resend Code</a>
        </div>
      </form>
    </div>
  </div>
  <div class="spacer"></div>
@endsection
