@extends('layouts.app')

@section('content')
  <div class="panel panel-default">
    <div class="panel-heading"><strong>Sign up for the Entergy Bid Board</strong></div>
    <div class="panel-body">
      <form role="form" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Your full name..." required autofocus>
          @if ($errors->has('name'))
          <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Your Entergy email..." required>
          @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
          @else
          <p class="help-block">This must be an <strong>@entergy.com</strong> email address</p>
          @endif
        </div>

        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
          <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Your valid phone number...">
          @if ($errors->has('phone'))
            <span class="help-block">
            <strong>{{ $errors->first('phone') }}</strong>
          </span>
          @else
            <p class="help-block">Only required if you would like to receive text notifications</p>
          @endif
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <input id="password" type="password" class="form-control" name="password" placeholder="Pick a password..." required>
          @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
          @else
          <p class="help-block">This should be <strong>different</strong> than your Entergy password</p>
          @endif
        </div>

        <div class="form-group">
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm your password..." required>
        </div>

        <div class="form-group">
          <p>
            <button type="submit" class="btn btn-primary btn-block btn-lg">Sign Up</button>
          </p>
        </div>
      </form>
    </div>
  </div>
  <p class="text-center">Already have an account? <a href="{{ route('login') }}">Log in</a></p>
@endsection
