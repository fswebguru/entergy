@extends('layouts.app')

@section('content')
  <div class="panel panel-default">
    <div class="panel-heading"><strong>Reset my password</strong></div>

    <div class="panel-body">
      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif

      <form role="form" method="POST" action="{{ route('password.request') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <input id="email" type="email" class="form-control input-lg" name="email" value="{{ $email or old('email') }}" placeholder="Your email address..." required autofocus>
          @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group">
          <p>
            <button type="submit" class="btn btn-primary btn-block btn-lg">Email Me a New Password</button>
          </p>
        </div>
      </form>
    </div>
  </div>
  <p class="text-center">Don't have an account? <a href="{{ route('register') }}">Sign Up</a></p>
@endsection
