<div class="pull-left" style="margin-right: 20px;">
  {!! BootForm::open()->post()->addClass('form-inline')->action(Request::url(), ['id' => 1]) !!}
    {!! BootForm::select('Sort by &nbsp;', 'sort-by')->options([
        'newest-date' => 'Date: newest first',
        'oldest-date' => 'Date: oldest first',
        'lowest-price' => 'Price: low to high',
        'highest-price' => 'Price: high to low'
      ])->select($sort_by)->onchange('this.form.submit()') !!}
  {!! BootForm::close() !!}
</div>

@if (isset($activeId))
<div class="pull-left">
  <div class="dropdown dropdown-form">
    <button type="button" class="btn @if (is_null($category_watch)) btn-primary @else btn-success @endif"
            id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
      <span class="glyphicon glyphicon-ok @if (is_null($category_watch)) hidden @endif" aria-hidden="true"></span> Get Alerts
    </button>
    <form class="dropdown-menu" aria-labelledby="dropdownMenu1" method="POST" action="{{ Request::url() . '/alert' }}">
      {{ csrf_field() }}
      <div class="form-group">
        <label>
          <input type="checkbox" name="notify_email" @if (!is_null($category_watch) && $category_watch->notify_email) checked @endif>
          &nbsp;Get&nbsp;email&nbsp;notifications
        </label>
      </div>
      <div class="form-group">
        <label>
          <input type="checkbox" name="notify_phone" @if (!is_null($category_watch) && $category_watch->notify_phone) checked @endif
          @if (is_null(Auth::user()->phone)) disabled @endif>
          &nbsp;Get&nbsp;text&nbsp;notifications
        </label>
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success" value="Confirm">
        <input type="button" class="btn btn-default" value="Cancel" onclick="$('#dropdownMenu1').dropdown('toggle');">
      </div>
    </form>
  </div>
</div>
@endif

<div class="clearfix"></div>
