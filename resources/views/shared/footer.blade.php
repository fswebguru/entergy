<hr>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <p>
          <a href="http://www.entergy.com/privacy_legal/privacy.aspx" target="_blank">Privacy Policy</a> |
          <a href="http://www.entergy.com/privacy_legal/legal.aspx" target="_blank">Terms &amp; Conditions</a>
          <br>&copy; 1998-{{ Carbon\Carbon::now()->format('Y') }} Entergy Corporation, All Rights Reserved.
          The Entergy name and logo are registered service marks of Entergy Corporation and may not be used without the express, written consent of Entergy
          Corporation.
        </p>
      </div>
    </div>
  </div>
</footer>
