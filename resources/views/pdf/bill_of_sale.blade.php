<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bill of Sale</title>

    <link rel="stylesheet" href="{{ asset('css/pdf.css') }}">

  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-sm-4 col-md-3 col-sm-offset-8 col-md-offset-9 pull-right">
          <p>
            <strong>ENTERGY SERVICES, INC.</strong><br/>
              INVESTMENT RECOVERY SERVICES<br/>
              Supply Chain Dept.<br/>
              639 Loyola Avenue<br/>
              New Orleans, LA 70113</p>
        </div>
      </div>

      <hr>
      <p class="date text-right"><strong>DATE: </strong>{{Carbon\Carbon::parse($ended_at)->format('M d, Y')}}</p>
      <h1 class="text-center">BILL OF SALE</h1>
      <p><strong>TO:</strong> WHOM IT MAY CONCERN</p>
      <p>THE INVESTMENT RECOVERY SERVICES OF ENTERGY SERVICES, INC. HEREBY SELLS THE FOLLOWING ITEM(S):</p>
      <p>Description of item(s) :</p>

      <table>
        <tbody>
          <tr>
            <th>Name</th>
            <th>Price</th>
          </tr>
          <tr>
            <td>{{$item_name}}</td>
            <td class="text-right">${{$amount}}</td>
          </tr>
          <tr>
            <td class="text-right"><strong>TOTAL</strong></td>
            <td class="text-right"><strong>${{$amount}}</strong></td>
          </tr>
        </tbody>
      </table>

      <p>THIS EQUIPMENT IS BEING SOLD “AS-IS, WHERE-IS,” WITH NO WARRANTY, EXPRESS OR IMPLIED, AS TO USEFULNESS OF MERCHANTABILITY. BUYER’S SIGNATURE BELOW ACKNOWLEDGES THAT THIS STATEMENT HAS BEEN READ AND ACCEPTED AS A CONDITION OF THE SALE.</p>

      <table>
        <tbody>
          <tr>
            <th colspan="2">BUYER AND SELLER DETAILS</th>
          </tr>
          <tr>
            <td class="shrink">BUYER</td>
            <td>
              {{isset($buyer_name) ? $buyer_name : $author_name}}
            </td>
          </tr>
          <tr>
            <td class="shrink">BUYER'S ADDRESS</td>
            <td class="underline expand"></td>
          </tr>
          <tr>
            <td class="shrink">BUYER'S SIGNATURE</td>
            <td class="underline expand signature"></td>
          </tr>
          <tr>
            <td class="shrink">DATE</td>
            <td class="underline expand"></td>
          </tr>
          <tr>
            <td class="shrink">SOLD BY</td>
            <td class="underline expand">Entergy Services, Inc.<br>639 Loyola Avenue<br>New Orleans, LA 70113</td>
          </tr>
          <tr>
            <td class="shrink">IR REP</td>
            <td class="underline expand">Justin McCabe (Entergy Investment Recovery Services)</td>
          </tr>
          <tr>
            <td class="shrink">SELLER SIGNATURE</td>
            <td class="underline expand signature">
              <img src="assets/pdf/sellersignature.png" alt="Seller signature" height="85">
            </td>
          </tr>
          <tr>
            <td class="shrink">DATE</td>
            <td class="underline expand">{{Carbon\Carbon::parse($ended_at)->format('M d, Y')}}</td>
          </tr>
        </tbody>
      </table>
      <p>The above signed is an authorized agent of Entergy Services, Inc. and is authorized to transact the sale for Entergy Services, located at the above address.</p>
    </div>
  </body>
</html>
