<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Entergy</title>
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
    </script>
  </head>
  <body>
  {{-- This is the Navbar, ideally it must change based on the User role--}}
  @include('shared.navbar')
  {{-- End of the Navbar. Main container starts --}}

    <div class="container" style="padding-top: 52px; padding-bottom: 80px;">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
          <h2>My Info</h2>
          <form role="form" method="POST" action="{{ route('user.profile') }}">
            {{ csrf_field() }}

            <hr>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <input type="text" class="form-control" id="name" name="name" placeholder="Your Name"
                     value="@if ($errors->has('name')){{ old('name') }}@else{{ $user->name }}@endif" required>
              @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
              <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone"
                     value="@if ($errors->has('phone')){{ old('phone') }}@elseif (!is_null($user->phone)){{ explode('+1', $user->phone)[1] }}@endif">
              @if ($errors->has('phone'))
              <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group">
              <input type="email" class="form-control" value="{{ $user->email }}" readonly>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <input type="password" class="form-control" id="password" name="password" placeholder="Pick a new password...">
              @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @else
              <p class="help-block">This should be <strong>different</strong> than your Entergy password</p>
              @endif
            </div>
            <hr>
            <p><button type="submit" class="btn btn-success">Update My Info</button></p>
          </form>
          <div class="spacer clearfix"></div>
        </div>
      </div>

      {{-- This is the footer section --}}
      @include('shared.footer')
      {{-- End of footer --}}
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
    <script>
        $(function() {
            $("#phone").mask("9999999999");
        });
    </script>
  </body>
</html>
