<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <title>Entergy</title>
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>
</head>
<body>
  {{-- This is the Navbar, ideally it must change based on the User role--}}
  @include('admin.navbar')
  {{-- End of the Navbar. Main container starts --}}
  <div class="container">

    <div class="spacer clearfix"></div>

    <div class="row">
      <div class="col-sm-12 col-md-4">
        <h1 style="margin: 0;">Auction Dashboard</h1>
        <div class="spacer clearfix show-xs"></div>
      </div>
      <div class="col-sm-6 col-md-5" id="buttons-bar">
      </div>
      <div class="col-sm-6 col-md-3 pull-right" id="date-filter-bar">
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <table class="orders" data-sort="table" id="dashboardTable">
          <thead>
          <tr role="heading">
            <th id="id_col" class="header">Id #</th>
            <th class="header">IR #</th>
            <th class="header"></th>
            <th class="header">Winner</th>
            <th class="header">Title</th>
            <th class="header">Category</th>
            <th class="header">Bus Unit</th>
            <th class="header">Bids</th>
            <th class="header">Close Date</th>
            <th class="header">Last bid</th>
          </tr>
          </thead>
          <tbody>
          @each('admin.auction-list-item', $auctions->sortByDesc('id'), 'auction')
          </tbody>
        </table>
      </div>
    </div>


    @include('shared.footer')
  </div>
  <script>
      $(document).ready(function () {
          // Datatable for /admin/dashboard
          var categories = [{text: 'All', className: 'category-filter'}];
          var busunits = [{text: 'All', className: 'busunit-filter'}];
          var table = $('#dashboardTable').DataTable({
              dom: '<"top"i>rt<"bottom"lp><"clear">',
              lengthChange: false,
              rowId: 'id_col',
              pageLength: 50,
              "order": [[0, "desc"]],
              initComplete: function () {
                  this.api().columns([5, 6]).every(function () {
                      var column = this;
                      if (column.index() == 5) {
                          column.data().unique().sort().each(function (d) {
                              categories.push({text: d, className: 'category-filter'});
                          });
                      } else if (column.index() == 6) {
                          column.data().unique().sort().each(function (d) {
                              busunits.push({text: d, className: 'busunit-filter'});
                          });
                      }
                  });
              },
          });

          yadcf.init(table, [
              {
                  column_number: 8,
                  date_format: "yyyy-mm-dd",
                  filter_type: "range_date",
                  filter_container_id: "date-filter-bar"
              }
          ]);

          var exportButtonContainer = $("#buttons-bar");
          if (exportButtonContainer.length > 0) {
              new $.fn.dataTable.Buttons(table, {
                  buttons: [
                      {
                          extend: 'collection',
                          text: 'Category <span class=\"caret\"></span>',
                          autoClose: true,
                          buttons: [categories]
                      },
                      {
                          extend: 'collection',
                          text: 'Bus Unit <span class=\"caret\"></span>',
                          autoClose: true,
                          buttons: [busunits]
                      },
                      {
                          extend: 'csv',
                          text: '<span class=\"glyphicon glyphicon-cloud-download\" aria-hidden=\"true\"></span> Export',
                          fieldSeparator: ',',
                          footer: false,
                          exportOptions: {
                              orthogonal: 'filter',
                              columns: [0, 1, 4, 5, 6, 7, 8, 9],
                              modifier: {
                                  search: 'none'
                              }
                          }
                      }
                  ],
              });
              table.buttons().container().appendTo(exportButtonContainer);

              $(".yadcf-filter-reset-button").html('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>');
          }

          $(document).on('click', '.category-filter', function () {
              var val = $.fn.dataTable.util.escapeRegex(
                  $(this).text()
              );
              if (val == 'All') {
                  table.columns(5).search('').draw();
              } else {
                  table.columns(5).search(val ? '^' + val + '$' : '', true, false).draw();
              }
          });
          $(document).on('click', '.busunit-filter', function () {
              var val = $.fn.dataTable.util.escapeRegex(
                  $(this).text()
              );
              if (val == 'All') {
                  table.columns(6).search('').draw();
              } else {
                  table.columns(6).search(val ? '^' + val + '$' : '', true, false).draw();
              }
          });
          $("#ui-datepicker-div").hide();
          $(".dt-button-background").trigger("click");
          $(".dt-button").trigger("click");
      });
  </script>
</body>
</html>
